from st3m.application import Application, ApplicationContext
from st3m.goose import Tuple, Optional
from st3m.input import InputState
from ctx import Context
import leds
import math
import random

SIM = False
try:
    import bl00mbox
except Exception as e:
    SIM = True


class Mass:
    """
    A point mass.

    Its mass is currently unused, all masses have the same unit mass :).
    """
    __slots__ = ("pos", "mass", "npos", "ppos")

    def __init__(self, x: float = 0, y: float = 0, mass: float = 1.0):
        self.pos = (x, y)
        self.npos = self.pos
        self.ppos = self.pos
        self.mass = mass

    def think(
        self,
        ins: InputState,
        delta_ms: int,
        gravity: Tuple[float, float],
        friction: float,
    ) -> None:
        if delta_ms > 100:
            return
        d = delta_ms / 1000
        vx = self.pos[0] - self.ppos[0]
        vy = self.pos[1] - self.ppos[1]

        if vy > 0:
            vy -= d * friction
        else:
            vy += d * friction
        if vx > 0:
            vx -= d * friction
        else:
            vx += d * friction

        vx += (self.mass * (gravity[0] * (d * d)) / 2) * 100
        vy += (self.mass * (gravity[1] * (d * d)) / 2) * 100
        self.npos = (
            self.pos[0] + vx,
            self.pos[1] + vy,
        )

    def apply(self) -> None:
        self.ppos = self.pos
        self.pos = self.npos

    def draw(self, ctx: Context) -> None:
        ctx.save()
        w, h = 5, 5
        x, y = self.pos
        ctx.rectangle(x - w / 2, y - w / 2, w, h)
        ctx.rgb(1, 0, 0)
        ctx.fill()
        ctx.restore()

    def __repr__(self) -> str:
        return f"<Mass {self.npos[0]},{self.npos[1]} {self.mass}>"


class Spring:
    """
    An ideal spring/distance constraint.
    """
    __slots__ = ("a", "b", "r")

    def __init__(self, a: Mass, b: Mass) -> None:
        self.a = a
        self.b = b
        dx = a.pos[0] - b.pos[0]
        dy = a.pos[1] - b.pos[1]
        self.r = math.sqrt(dx * dx + dy * dy)

    def draw(self, ctx: Context) -> None:
        ctx.save()
        ctx.rgb(0, 1, 0)
        ctx.line_width = 2
        ctx.move_to(self.a.pos[0], self.a.pos[1])
        ctx.line_to(self.b.pos[0], self.b.pos[1])
        ctx.stroke()
        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> float:
        a, b = self.a, self.b
        dx = a.npos[0] - b.npos[0]
        dy = a.npos[1] - b.npos[1]
        d1 = math.sqrt(dx * dx + dy * dy)
        d2 = (d1 - self.r) / d1

        self.a.npos = (
            a.npos[0] - 0.5 * dx * d2,
            a.npos[1] - 0.5 * dy * d2,
        )
        self.b.npos = (
            b.npos[0] + 0.5 * dx * d2,
            b.npos[1] + 0.5 * dy * d2,
        )
        return d2


class NickApp(Application):
    """
    A simple Verlet Integration based physics simulation of a box covered in
    q3k's avatar.

    Jank, does not correctly handle variable time steps. Should be rewritten to
    proper velocity verlet.
    """
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._phase = 0.0
        self._masses = [
            Mass(-50, -50, 1),
            Mass(-50, 50, 1),
            Mass(50, 50, 1),
            Mass(50, -50, 1),
        ]
        self._impact_timeout: Optional[int] = None
        self._springs = [
            Spring(self._masses[0], self._masses[1]),
            Spring(self._masses[1], self._masses[2]),
            Spring(self._masses[2], self._masses[3]),
            Spring(self._masses[3], self._masses[0]),
            Spring(self._masses[0], self._masses[2]),
            Spring(self._masses[1], self._masses[3]),
        ]
        if not SIM:
            self._blm = bl00mbox.Channel("q3knick")
            names = [
                "body_medium_impact_soft1.wav",
                "body_medium_impact_soft5.wav",
                "body_medium_impact_soft6.wav",
                "body_medium_impact_soft7.wav",
            ]
            self._samples = []
            for name in names:
                path = app_ctx.bundle_path + "/" + name
                print(path)
                sample = self._blm.new(bl00mbox.patches.sampler, path)
                sample.signals.output = self._blm.mixer
                self._samples.append(sample)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        draw_debug = False
        if draw_debug:
            for m in self._masses:
                m.draw(ctx)
            for c in self._springs:
                c.draw(ctx)

        m0 = self._masses[0]
        m1 = self._masses[1]

        dx = m0.pos[0] - m1.pos[0]
        dy = m0.pos[1] - m1.pos[1]
        angle = math.atan2(dy, dx)

        ctx.save()
        ctx.translate(m0.pos[0], m0.pos[1])
        ctx.rotate(angle + 3.1415 / 2)

        ctx.rectangle(0, 0, 100, 100)
        ctx.gray(0.145)
        ctx.fill()

        ctx.font = "Arimo Regular"
        ctx.move_to(30, 70)
        ctx.font_size = 40
        ctx.rgb(1, 1, 1)
        ctx.text_align = ctx.LEFT
        ctx.text_baseline = ctx.MIDDLE
        ctx.text("q3k")
        ctx.restore()

    def _ragdoll(self) -> None:
        """
        haha funny noise
        """
        if SIM:
            print("splat")
            return
        if self._impact_timeout is None:
            sample = random.choice(self._samples)
            sample.signals.trigger.start()
            self._impact_timeout = 100

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self._impact_timeout is not None:
            self._impact_timeout -= delta_ms
            if self._impact_timeout < 0:
                self._impact_timeout = None

        iy = ins.imu.acc[0] * 5
        ix = ins.imu.acc[1] * 5
        gravity = (ix, iy)

        for m in self._masses:
            x, y = m.pos
            d = math.sqrt(x * x + y * y)
            friction = 1.0
            if d > 120:
                friction = 10.0
            m.think(ins, delta_ms, gravity, friction)
            x, y = m.npos
            d = math.sqrt(x * x + y * y)
            if d > 120:
                fixup = d / 120
                x /= fixup
                y /= fixup
                m.npos = (x, y)
        for s in self._springs:
            v = s.think(ins, delta_ms)
            if v > 0.05 or v < -0.05:
                self._ragdoll()
        for m in self._masses:
            m.apply()

        self._phase += delta_ms / 1000

        p = self._phase * 10
        for i in range(40):
            if (int(i + p) * 2) % 40 < 20:
                leds.set_rgb(i, 255, 255, 255)
            else:
                leds.set_rgb(i, 0, 0, 0)
        leds.update()


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(NickApp(ApplicationContext()))
